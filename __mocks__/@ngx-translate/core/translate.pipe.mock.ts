import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'translate'
})
export class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    if (args.length > 0) {
      return `[translated]${query}:${args.map(arg => JSON.stringify(arg)).join('|')}`;
    }
    return `[translated]${query}`;
  }
}
