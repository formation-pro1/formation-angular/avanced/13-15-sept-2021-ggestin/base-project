import {of} from 'rxjs';
import fn = jest.fn;

export let translateServiceMock  = {
  get: fn((key) => of(`[translated]${key}`)),
  setDefaultLang: fn((arg) => {}),
  use: fn((arg) => {}),
  instant: fn((key) => `[translated]${key}`),
  addLangs: fn((arg) => {}),
  getLangs: fn((arg) => ['en', 'fr']),
  getBrowserLang: fn((arg) => 'en'),
  currentLang: 'fr',
};
