import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";
import {ComponentFixture} from "@angular/core/testing";

export const getNativeByCss = (fixture: ComponentFixture<any>, cssSelector: string): HTMLElement | null => {
  const deComp: DebugElement = fixture.debugElement;
  const deElement = deComp.query(By.css(cssSelector))
  if (deElement === null) {
    return null;
  }
  return deElement.nativeElement;
}
