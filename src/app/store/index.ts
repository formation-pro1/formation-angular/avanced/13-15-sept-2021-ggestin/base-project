import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromClient from './reducers/client.reducer';
import * as fromOrder from './reducers/order.reducer';


export interface AppState {

  [fromClient.clientFeatureKey]: fromClient.State;
  [fromOrder.orderFeatureKey]: fromOrder.State;
}

export const reducers: ActionReducerMap<AppState> = {

  [fromClient.clientFeatureKey]: fromClient.reducer,
  [fromOrder.orderFeatureKey]: fromOrder.reducer,
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
