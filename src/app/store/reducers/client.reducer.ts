import {createReducer, on} from '@ngrx/store';
import {Client} from "../../core/models/client";
import * as clientActions from '../actions/client.actions';


export const clientFeatureKey = 'client';

export interface State {
  data: Client[];
  loading: boolean;
  errors: any[];
  toUpdate: Client | null;
}

export const initialState: State = {
  data: [],
  loading: false,
  errors: [],
  toUpdate: null
};

export const reducer = createReducer(
  initialState,
  on(clientActions.loadClients, (state) => ({...state, loading: true})),
  on(clientActions.loadClientsSuccess, (state, {data}) => ({...state, data, loading: false})),

  on(clientActions.loadClient, (state) => ({...state, loading: true, toUpdate: null})),
  on(clientActions.loadClientSuccess, (state, {data}) => ({...state, loading: false, toUpdate: data})),

  on(clientActions.createClient, (state) => ({...state, loading: true})),
  // on(clientActions.createClient, (state, data: { payload: Client }) => ({...state, data: [...state.data, data.payload]}))
  on(clientActions.createClientSuccess, (state) => ({...state, loading: false})),
  // on(clientActions.createClientSuccess, (state, { data }) => ({...state, loading: false, data: [...state.data, data]})),

  on(clientActions.updateClient, (state) => ({...state, loading: true})),
  on(clientActions.updateClientSuccessWithRedirect, (state) => ({...state, loading: false})),
  on(clientActions.updateClientSuccessWithoutRedirect, (state) => ({...state, loading: false})),

  on(clientActions.deleteClient, state => ({ ...state, loading: true })),
  on(clientActions.deleteClientSuccess, state => ({...state, loading: false})),
  /*
    on(ClientsActions.deleteClientSuccess, (state, {id}) => ({
      ...state, loading: false, loaded: true, data: state.data.filter(client => id !== client.id)
    })),
   */
  on(clientActions.clientFailureGeneric, (state, {error}) => ({
    ...state,
    loading: false,
    errors: [...state.errors, error]
  })),
);
