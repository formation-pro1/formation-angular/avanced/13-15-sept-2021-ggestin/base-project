import {createReducer, on} from '@ngrx/store';
import * as clientOrders from "../actions/order.actions";
import {Order} from "../../core/models/order";
import {StateOrder} from "../../core/enums/state-order";
import {OrderI} from "../../core/interfaces/order-i";


export const orderFeatureKey = 'order';

const mockData: OrderI = new Order({
  "id": 1,
  "tjmHt": 1200,
  "nbJours": 20,
  "tva": 20,
  "state": StateOrder.CANCELED,
  "typePresta": "formation",
  "client": "Capgemini2",
  "comment": "Merci Cap pour les 24k"
});

export interface State {
  data: Order[]
}

export const initialState: State = {
  data: []
};


export const reducer = createReducer(
  initialState,
  on(clientOrders.loadOrders, (state) => ({...state, data: [...state.data, mockData]}))
);
