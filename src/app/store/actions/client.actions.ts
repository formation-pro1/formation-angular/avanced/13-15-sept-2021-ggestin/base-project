import { createAction, props } from '@ngrx/store';
import {Client} from "../../core/models/client";

export const loadClients = createAction(
  '[Client] Load Clients'
);
export const loadClientsSuccess = createAction(
  '[Client] Load Clients Success',
  props<{ data: Client[] }>()
);

export const loadClient = createAction(
  '[Client] Load Client',
  props<{ id: number }>()
);
export const loadClientSuccess = createAction(
  '[Client] Load Client Success',
  props<{ data: Client }>()
);

export const createClient = createAction(
  '[Client] Create Client',
  props<{ payload: Client }>()
);
export const createClientSuccess = createAction(
  '[Client] Create Client Success'
);

export const updateClient = createAction(
  '[Client] Update Client',
  props<{ payload: Client, redirect: boolean }>()
);
export const updateClientSuccessWithRedirect = createAction(
  '[Client] Update Client Success redirect'
);
export const updateClientSuccessWithoutRedirect = createAction(
  '[Client] Update Client Success reload'
);

export const deleteClient = createAction(
  '[Client] Delete Client',
  props<{ payload: Client }>()
);
export const deleteClientSuccess = createAction(
  '[Client] Delete Client Success'
);
export const clientFailureGeneric = createAction(
  '[Client] Client Failure Generic',
  props<{ error: any }>()
);
