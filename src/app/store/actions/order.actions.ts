import {Action, createAction} from '@ngrx/store';

export const loadOrders = createAction(
  '[Order] Load Orders'
);

export const postOrders = createAction(
  '[Order] Post Orders'
);


