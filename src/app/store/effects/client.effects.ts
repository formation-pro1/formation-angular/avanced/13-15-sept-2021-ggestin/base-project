import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as fromClient from "../actions/client.actions";
import {catchError, map, switchMap, tap} from "rxjs/operators";
import {ClientService} from "../../clients/services/client.service";
import {Client} from "../../core/models/client";
import {of} from "rxjs";
import {Router} from "@angular/router";

@Injectable()
export class ClientEffects {

  constructor(
    private actions$: Actions,
    private router: Router,
    private clientService: ClientService
  ) {}

  loadClients$ = createEffect(
    () => this.actions$.pipe(
      ofType(fromClient.loadClients),
      switchMap(() => this.clientService.list()),
      map((data: Client[]) => fromClient.loadClientsSuccess({ data })),
      catchError((error) => of(fromClient.clientFailureGeneric({ error })))
    )
  )

  loadClient$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromClient.loadClient),
        switchMap(({ id }) => this.clientService.get(id)),
        map((data: Client) => fromClient.loadClientSuccess({ data })),
        catchError((error) => of(fromClient.clientFailureGeneric({ error })))
      )
  );

  createClients$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromClient.createClient),
        switchMap(({ payload }) => this.clientService.create(payload)),
        map(() => fromClient.createClientSuccess()),
        catchError((error) => of(fromClient.clientFailureGeneric({ error })))
      )
  );

  updateClients$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromClient.updateClient),
        switchMap(({ payload, redirect }) => of({
            data: this.clientService.update(payload),
            redirect
        })),
        map(({redirect}) => redirect ? fromClient.updateClientSuccessWithRedirect() : fromClient.updateClientSuccessWithoutRedirect()),
        catchError((error) => of(fromClient.clientFailureGeneric({ error })))
      )
  );

  deleteClients$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromClient.deleteClient),
        switchMap(({ payload }) => this.clientService.delete(payload)),
        map(() => fromClient.deleteClientSuccess()),
        catchError((error) => of(fromClient.clientFailureGeneric({ error })))
      )
  );

  redirectToClientList$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromClient.createClientSuccess, fromClient.updateClientSuccessWithRedirect),
        tap(() => this.router.navigate(['clients']))
      ), {
      dispatch: false
    }
  );

  reloadClientList$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromClient.deleteClientSuccess, fromClient.updateClientSuccessWithoutRedirect),
        map(() => fromClient.loadClients())
      )
  );
}
