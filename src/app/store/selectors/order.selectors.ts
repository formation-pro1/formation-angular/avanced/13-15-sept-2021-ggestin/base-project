import { createFeatureSelector, createSelector } from '@ngrx/store';

import {AppState} from "../index";
import * as fromOrder from "../reducers/order.reducer";

export const selectFeature = (state: AppState) => state[fromOrder.orderFeatureKey];

export const selectOrderList = createSelector(
  selectFeature,
  (state: fromOrder.State) => state.data
);
