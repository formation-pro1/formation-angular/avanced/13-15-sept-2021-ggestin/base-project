import { createFeatureSelector, createSelector } from '@ngrx/store';

import {AppState} from '../index';
import * as fromClient from '../reducers/client.reducer';

export const selectFeature = (state: AppState) => state[fromClient.clientFeatureKey];

export const selectClientList = createSelector(
  selectFeature,
  (state: fromClient.State) => state.data
);

export const selectClientToUpdate = createSelector(
  selectFeature,
  (state: fromClient.State) => state.toUpdate
);
