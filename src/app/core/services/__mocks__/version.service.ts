import {BehaviorSubject, of} from 'rxjs';

export const versionServiceMock  = {
  incrementVersion: jest.fn(() => {}),
  numVersion: new BehaviorSubject(20)
}
