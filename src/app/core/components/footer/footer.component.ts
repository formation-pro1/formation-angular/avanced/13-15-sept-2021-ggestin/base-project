import { Component, OnInit } from '@angular/core';
import { VersionService } from '../../services/version.service';
import {TranslateService} from "@ngx-translate/core";
import {Observable} from "rxjs";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  public version!: number;

  constructor(
    private versionService: VersionService,
    private translateService: TranslateService
  ) {
    this.versionService.numVersion.subscribe((next) => this.version = next);
  }

  use(lang: string): Observable<any> {
    return this.translateService.use(lang)
  }

  currentLang(): string {
    return this.translateService.currentLang ?? this.translateService.defaultLang
  }

  getLangs(): string[] {
    return this.translateService.getLangs();
  }
}
