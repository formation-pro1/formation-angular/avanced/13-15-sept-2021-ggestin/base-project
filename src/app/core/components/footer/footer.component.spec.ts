import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {FooterComponent} from './footer.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {VersionService} from "../../services/version.service";
import {versionServiceMock} from "../../services/__mocks__/version.service";
import {translateServiceMock} from "../../../../../__mocks__/@ngx-translate/core/translate.service.mock";

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FooterComponent],
      imports: [
        NgxTranslateTestingModule
      ],
      providers: [{provide: VersionService, useValue: versionServiceMock}]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => jest.clearAllMocks());

  describe('Init test', () => {
    test('should create', waitForAsync(() => {
      expect(component).toBeTruthy();
      versionServiceMock.numVersion.next(19);
      expect(component.version).toEqual(19);
    }));
  });
  describe('Typescript test', () => {
    test('should call translateService#use', () => {
      component.use('toto');
      expect(translateServiceMock.use).toHaveBeenNthCalledWith(1, 'toto')
    });
    describe('[CURRENT LANG]', () => {
      test('should return the current lang', () => {
        expect(component.currentLang()).toStrictEqual(translateServiceMock.currentLang)
      });
    });
  });
});
