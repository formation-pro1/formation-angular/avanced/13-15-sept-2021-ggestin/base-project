import {Component, OnInit} from '@angular/core';
import {VersionService} from '../../services/version.service';
import {AuthService} from "../../../shared/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public version!: number;
  public param: { username: string } = {username: ''};
  public userConnected$ = this.authService.isLogged$;

  constructor(
    private versionService: VersionService,
    private authService: AuthService
  ) {
    this.versionService.numVersion
      .subscribe((next) => this.version = next);
  }

  ngOnInit(): void {
    this.authService.user$.subscribe((user) => {
      // if (user !== null && user.login !== null) {
      //   this.param = {username: user.login}
      // } else {
      //   this.param = {username: ''}
      // }
      this.param = {username: user?.login ?? ''}
    })
  }

  logout() {
    this.authService.logout();
  }
}
