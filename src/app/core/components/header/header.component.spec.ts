import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {HeaderComponent} from './header.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthService} from "../../../shared/auth.service";
import {authServiceMock} from "../../../shared/__mocks__/auth.service";
import {VersionService} from "../../services/version.service";
import {versionServiceMock} from "../../services/__mocks__/version.service";
import {getNativeByCss} from "../../../../testUtils/templateTestUtils";

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [
        RouterTestingModule,
        NgxTranslateTestingModule
      ],
      providers: [
        {provide: AuthService, useValue: authServiceMock},
        {provide: VersionService, useValue: versionServiceMock}
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Template test', () => {
    test('should display a translated title', () => {
      const e: HTMLElement | null = getNativeByCss(fixture, 'h1');
      // @ts-ignore
      expect(e.textContent.trim()).toStrictEqual('[translated]HEADER.TITLE')
    });
    describe('User connected', () => {
      beforeEach(waitForAsync(() =>
        component.userConnected$.subscribe(userConnected => expect(userConnected).toBeTruthy())
      ));
      test('should display welcome message', waitForAsync(() => {
        const e: HTMLElement | null = getNativeByCss(fixture, 'span');
        // @ts-ignore
        expect(e.textContent.trim()).toStrictEqual('[translated]HEADER.WELCOME:{\"username\":\"loginTest\"}');
      }));
      test('should not display login button', waitForAsync(() => {
        expect(getNativeByCss(fixture, 'button')).toBeNull();
      }));
    });
    describe('User not connected', () => {
      beforeEach(waitForAsync(() => {
        authServiceMock.isLogged$.next(false);
        fixture.detectChanges()
        component.userConnected$.subscribe(userConnected => expect(userConnected).toBeFalsy())
      }));
      test('should display no welcome message', waitForAsync(() => {
        expect(getNativeByCss(fixture, 'span')).toBeNull();
      }));
      test('should display login button', waitForAsync(() => {
        const e: HTMLButtonElement | null = getNativeByCss(fixture, 'button') as HTMLButtonElement;
        // @ts-ignore
        expect(e.textContent.trim()).toStrictEqual('[translated]HEADER.LOGIN_BTN');
      }));
    });

    describe('Snapshot test', () => {
      test('User connected', () => {
        component.userConnected$.subscribe(userConnected => expect(userConnected).toBeTruthy())
        expect(fixture).toMatchSnapshot();
      });
      test('User not connected', () => {
        authServiceMock.isLogged$.next(false);
        fixture.detectChanges()
        component.userConnected$.subscribe(userConnected => expect(userConnected).toBeFalsy())
        expect(fixture).toMatchSnapshot();
      });
    });
  });
});
