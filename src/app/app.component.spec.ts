import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {MockIconCloseComponent} from "./icons/components/__mocks__/icon-close.component";
import {MockNavComponent} from "./core/components/__mocks__/nav.component";
import {MockHeaderComponent} from "./core/components/__mocks__/header.component";
import {MockFooterComponent} from "./core/components/__mocks__/footer.component";
import {MockIconNavComponent} from "./icons/components/__mocks__/icon-nav.component";
import {MockUiComponent} from "./ui/components/__mocks__/ui.component";
import {NgxTranslateTestingModule} from "../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {translateServiceMock} from "../../__mocks__/@ngx-translate/core/translate.service.mock";

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgxTranslateTestingModule
      ],
      declarations: [
        AppComponent,
        MockUiComponent,
        MockNavComponent,
        MockHeaderComponent,
        MockFooterComponent,
        MockIconNavComponent,
        MockIconCloseComponent
      ],
    }).compileComponents();
  });

  afterEach(() => jest.clearAllMocks());

  describe('Init test', () => {
    test('should create the app', () => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      expect(app).toBeTruthy();
    });
    test('should init the translate lang from getBrowserLang', () => {
      TestBed.createComponent(AppComponent);
      expect(translateServiceMock.addLangs).toHaveBeenNthCalledWith(1, ['en', 'fr', 'esp'])
      expect(translateServiceMock.setDefaultLang).toHaveBeenNthCalledWith(1, 'fr')
      expect(translateServiceMock.getBrowserLang).toHaveBeenCalledTimes(1)
      expect(translateServiceMock.use).toHaveBeenNthCalledWith(1, 'en')
    });
    test('should init the translate lang with getBrowserLang not managed', () => {
      translateServiceMock.getBrowserLang.mockImplementationOnce(() => 'bg')

      TestBed.createComponent(AppComponent);
      expect(translateServiceMock.addLangs).toHaveBeenNthCalledWith(1, ['en', 'fr', 'esp'])
      expect(translateServiceMock.setDefaultLang).toHaveBeenNthCalledWith(1, 'fr')
      expect(translateServiceMock.getBrowserLang).toHaveBeenCalledTimes(1)
      expect(translateServiceMock.use).toHaveBeenNthCalledWith(1, 'fr')
    });
  });
});
