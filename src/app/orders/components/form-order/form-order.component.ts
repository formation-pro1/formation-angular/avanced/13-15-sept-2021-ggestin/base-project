import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { StateOrder } from 'src/app/core/enums/state-order';
import { Order } from 'src/app/core/models/order';
import {HasFormDirty} from "../../../shared/guards/check-form.guard";

@Component({
  selector: 'app-form-order',
  templateUrl: './form-order.component.html',
  styleUrls: ['./form-order.component.scss']
})
export class FormOrderComponent extends HasFormDirty implements OnInit {

  public states = Object.values(StateOrder);

  @Input()
  public order!: Order;

  @Output()
  public submitted = new EventEmitter<Order>();

  constructor(
    private fb: FormBuilder
  ) {
    super()
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      tjmHt: [this.order.tjmHt],
      nbJours: [this.order.nbJours],
      tva: [this.order.tva],
      state: [this.order.state],
      typePresta: [this.order.typePresta, Validators.required],
      client: [this.order.client, [Validators.required, Validators.minLength(2)]],
      comment: [this.order.comment],
      id: [this.order.id]
    });
  }

  public onSubmit(): void {
    this.form.markAsPristine();
    this.submitted.emit(this.form.value);
  }
}
