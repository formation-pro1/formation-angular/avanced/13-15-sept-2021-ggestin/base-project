import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageAddOrderComponent} from './pages/page-add-order/page-add-order.component';
import {PageEditOrderComponent} from './pages/page-edit-order/page-edit-order.component';
import {PageListOrdersComponent} from './pages/page-list-orders/page-list-orders.component';
import {CheckFormGuard} from "../shared/guards/check-form.guard";
import {GetOrdersResolver} from "./resolvers/get-orders.resolver";
import {GetOrderResolver} from "./resolvers/get-order.resolver";

const routes: Routes = [
  {
    path: '', component: PageListOrdersComponent, resolve: {
      list: GetOrdersResolver
    }
  },
  {path: 'add', component: PageAddOrderComponent, canDeactivate: [CheckFormGuard]},
  {
    path: 'edit/:id',
    component: PageEditOrderComponent,
    canDeactivate: [CheckFormGuard],
    resolve: {order: GetOrderResolver}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule {
}
