import {Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { Order } from 'src/app/core/models/order';
import { OrdersService } from '../../services/orders.service';
import {FormOrderComponent} from "../../components/form-order/form-order.component";
import {CheckForm} from "../../../shared/guards/check-form.guard";

@Component({
  selector: 'app-page-add-order',
  templateUrl: './page-add-order.component.html',
  styleUrls: ['./page-add-order.component.scss'],
})
export class PageAddOrderComponent extends CheckForm<FormOrderComponent> {

  public item = new Order();

  constructor(
    private orderService: OrdersService,
    private router: Router
  ) {
    super()
  }

  public saveOrder(order: Order): void {
    this.orderService.add(order).subscribe(
      () => {
        this.router.navigate(['orders']);
      }
    )
  }
}
