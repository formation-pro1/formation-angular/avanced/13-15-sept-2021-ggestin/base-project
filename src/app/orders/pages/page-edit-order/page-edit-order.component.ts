import {Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Order } from 'src/app/core/models/order';
import { OrdersService } from '../../services/orders.service';
import {FormOrderComponent} from "../../components/form-order/form-order.component";
import {CheckForm} from "../../../shared/guards/check-form.guard";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-page-edit-order',
  templateUrl: './page-edit-order.component.html',
  styleUrls: ['./page-edit-order.component.scss']
})
export class PageEditOrderComponent extends CheckForm<FormOrderComponent> implements OnInit{

  public order$!: Observable<Order>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private ordersService: OrdersService
  ) {
    super()
  }

  ngOnInit(): void {
    this.order$ = this.activatedRoute.data.pipe(map(data => data.order ))
  }

  public updateOrder(order: Order): void {
    this.ordersService.update(order).subscribe(() => this.router.navigate(['orders']))
  }
}
