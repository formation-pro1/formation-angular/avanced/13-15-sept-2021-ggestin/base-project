import {Injectable} from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {EMPTY, Observable, throwError} from 'rxjs';
import {OrdersService} from "../services/orders.service";
import {Order} from "../../core/models/order";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class GetOrderResolver implements Resolve<Order> {

  constructor(
    private ordersService: OrdersService
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Order> {
    return this.ordersService.getOrderById(Number(route.paramMap.get('id')))
      .pipe(catchError(err => {
        console.error(`L'order ${Number(route.paramMap.get('id'))} n'existe pas`);
        return EMPTY
      }));
  }
}
