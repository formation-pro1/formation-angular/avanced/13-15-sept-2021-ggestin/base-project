import {Injectable} from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {Observable} from 'rxjs';
import {Order} from "../../core/models/order";
import * as orderActions from "../../store/actions/order.actions";
import {select, Store} from "@ngrx/store";
import {AppState} from "../../store";
import {selectOrderList} from "../../store/selectors/order.selectors";
import {first, take} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class GetOrdersResolver implements Resolve<Order[]> {

  constructor(
    private store: Store<AppState>
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Order[]> {
    this.store.dispatch(orderActions.loadOrders())
    return this.store.pipe(select(selectOrderList), take(1));
    // return this.store.pipe(select(selectOrderList), first());
  }
}
