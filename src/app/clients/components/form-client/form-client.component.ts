import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClientState} from '../../../core/enums/state-client';
import {Client} from "../../../core/models/client";

@Component({
  selector: 'app-form-client',
  templateUrl: './form-client.component.html',
  styles: [`
    form {
      .ng-valid.ng-touched {
        border: 3px solid var(--app-success);
      }

      .ng-invalid.ng-touched {
        border: 3px solid var(--app-error);
      }
    }
  `]
})
export class FormClientComponent implements OnInit {
  form!: FormGroup;
  @Input() initClient = {name: '', email: '', state: 'Active'};
  @Output() submitted: EventEmitter<Client> = new EventEmitter<Client>();
  clientsStates = Object.values(ClientState);

  constructor(
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
        name: [this.initClient.name, Validators.required],
        email: [this.initClient.email, Validators.email],
        state: this.initClient.state
      }
    );
  }

  onSubmit() {
    this.submitted.emit(this.form.value);
  }
}
