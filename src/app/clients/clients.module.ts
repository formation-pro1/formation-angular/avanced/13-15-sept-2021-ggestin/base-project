import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientsRoutingModule } from './clients-routing.module';
import { PageAddClientComponent } from './pages/page-add-client/page-add-client.component';
import { PageEditClientComponent } from './pages/page-edit-client/page-edit-client.component';
import { PageListClientsComponent } from './pages/page-list-clients/page-list-clients.component';
import { SharedModule } from '../shared/shared.module';
import {TranslateModule} from "@ngx-translate/core";
import { ClientLayoutFullComponent } from './layout/client-layout-full/client-layout-full.component';
import { FormClientComponent } from './components/form-client/form-client.component';

@NgModule({
  declarations: [
    PageAddClientComponent,
    PageEditClientComponent,
    PageListClientsComponent,
    ClientLayoutFullComponent,
    FormClientComponent
  ],
    imports: [
        CommonModule,
        ClientsRoutingModule,
        SharedModule,
        TranslateModule
    ]
})
export class ClientsModule { }
