import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot} from '@angular/router';
import {Client} from "../../../core/models/client";
import {Observable} from "rxjs";
import {loadClient, updateClient} from "../../../store/actions/client.actions";
import {select, Store} from "@ngrx/store";
import {AppState} from "../../../store";
import {selectClientToUpdate} from "../../../store/selectors/client.selectors";

@Component({
  selector: 'app-page-edit-client',
  template: `
    <h1> Edition d'un client</h1>
    <app-form-client
      *ngIf="client$ | async as client"
      [initClient]="client"
      (submitted)="editClient($event)"
    >
    </app-form-client>
  `
})
export class PageEditClientComponent implements OnInit {
  public client$: Observable<Client | null> = this.store.pipe(select(selectClientToUpdate));

  private get id(): number {
    return Number(this.route.snapshot.paramMap.get('id'))
  }

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.store.dispatch(loadClient({id: this.id}))
  }

  editClient(payload: Client): void {
    this.store.dispatch(updateClient({payload: {...payload, id: this.id}, redirect: true}))
  }
}
