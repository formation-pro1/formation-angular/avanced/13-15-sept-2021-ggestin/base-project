import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {AppState} from '../../../store';
import {select, Store} from '@ngrx/store';
import {ClientState} from "../../../core/enums/state-client";
import {Client} from "../../../core/models/client";
import * as clientActions from '../../../store/actions/client.actions';
import {selectClientList} from "../../../store/selectors/client.selectors";
import {deleteClient, updateClient} from "../../../store/actions/client.actions";

@Component({
  selector: 'app-page-list-clients',
  templateUrl: './page-list-clients.component.html',
  styleUrls: ['./page-list-clients.component.scss']
})
export class PageListClientsComponent implements OnInit {
  headers = ['Actions', 'Name', 'Email', 'State'];
  clientsStates = Object.values(ClientState);

  public clients$!: Observable<Client[]>;

  constructor(
    private store: Store<AppState>,
    private router: Router
  ) {
    this.clients$ = this.store.pipe(select(selectClientList));
  }

  ngOnInit(): void {
    this.store.dispatch(clientActions.loadClients())
  }

  update(state: string, payload: Client) {
    this.store.dispatch(updateClient({payload: {...payload, state}, redirect: false}))
  }

  public navigateToEdit(id: number): void {
    this.router.navigate(['clients', 'edit', id]);
  }

  delete(payload: Client) {
    this.store.dispatch(deleteClient({ payload }))
  }
}
