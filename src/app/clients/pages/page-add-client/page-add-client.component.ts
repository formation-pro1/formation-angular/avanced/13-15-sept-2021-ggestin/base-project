import {Component} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store";
import {Client} from "../../../core/models/client";
import {createClient} from "../../../store/actions/client.actions";

@Component({
  selector: 'app-page-add-client',
  template: `
    <h1> Ajout un client</h1>
    <app-form-client (submitted)="addClient($event)"></app-form-client>
  `
})
export class PageAddClientComponent {

  constructor(
    private store: Store<AppState>
  ) {
  }

  addClient(payload: Client) {
    this.store.dispatch(createClient({ payload }))
  }
}
