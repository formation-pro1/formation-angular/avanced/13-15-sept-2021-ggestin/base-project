import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientLayoutFullComponent } from './client-layout-full.component';

describe('ClientLayoutFullComponent', () => {
  let component: ClientLayoutFullComponent;
  let fixture: ComponentFixture<ClientLayoutFullComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientLayoutFullComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientLayoutFullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
