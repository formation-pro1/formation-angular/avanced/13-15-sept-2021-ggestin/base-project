import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {map, startWith, switchMap} from "rxjs/operators";
import {Title} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-client-layout-full',
  template: `
    <div class="bandeau">
      <h1>{{ pageTitle }}</h1>
    </div>
    <div>
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [`
    .bandeau {
      background: var(--app-primary);
      padding: 1rem;

      h1 {
        color: var(--app-light);
      }
    }
  `]
})
export class ClientLayoutFullComponent implements OnInit {
  pageTitle!: string;

  constructor(
    private title: Title,
    private route: ActivatedRoute,
    private translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.translateService.onLangChange.pipe(
      startWith(this.translateService.currentLang)
    ).subscribe(() =>
      this.route.firstChild?.data
        .pipe(
          map(data => data.pageTitle),
          switchMap((key => this.translateService.get(`CLIENT.TITLE.${key}`)))
        )
        .subscribe(title => {
          this.pageTitle = title
          this.title.setTitle(title)
        })
    )
  }
}
