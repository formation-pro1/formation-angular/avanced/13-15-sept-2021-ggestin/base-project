import {TestBed, waitForAsync} from '@angular/core/testing';

import {ClientService} from './client.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Client} from "../../core/models/client";
import mock = jest.mock;

const mockData: Client = {
  state: 'Active',
  name: 'name',
  email: 'email',
  id: 1
}

describe('ClientService', () => {
  let service: ClientService;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(ClientService);
    controller = TestBed.inject(HttpTestingController);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    describe('[LIST]', () => {
      test('should return an empty list with "waitForAsync"', waitForAsync(() => {
        service.list().subscribe(list =>
          expect(list).toStrictEqual('clients[]')
        )

        const req = controller.expectOne('http://localhost:3000/clients')
        expect(req.request.method).toStrictEqual('GET')

        req.flush('clients[]')
      }));
      test('should return an empty list with "done"', (done) => {
        service.list().subscribe(list => {
          expect(list).toStrictEqual('clients[]');
          done();
        })

        const req = controller.expectOne('http://localhost:3000/clients')
        expect(req.request.method).toStrictEqual('GET')

        req.flush('clients[]')
      });
    });
    describe('[CREATE]', () => {
      test('should call a post on the endpoint', waitForAsync(() => {
        service.create(mockData).subscribe(next => {
          expect(next.body).toStrictEqual('newly created client')
          expect(next.status).toStrictEqual(201)
        })

        const req = controller.expectOne('http://localhost:3000/clients')
        expect(req.request.method).toStrictEqual('POST')
        expect(req.request.body).toStrictEqual(mockData)

        req.flush('newly created client', {
          status: 201,
          statusText: 'created'
        })
      }));
    });
    describe('[UPDATE]', () => {
      test('should call a put on the endpoint', waitForAsync(() => {
        service.update(mockData).subscribe(next => {
          expect(next).toStrictEqual('newly updated client')
        })

        const req = controller.expectOne(`http://localhost:3000/clients/${mockData.id}`)
        expect(req.request.method).toStrictEqual('PUT')
        expect(req.request.body).toStrictEqual(mockData)

        req.flush('newly updated client')
      }));
    });
    describe('[DELETE]', () => {
      test('should call a delete on the endpoint', waitForAsync(() => {
        service.delete(mockData).subscribe(response => {
          expect(response.status).toEqual(204)
        })

        const req = controller.expectOne(`http://localhost:3000/clients/${mockData.id}`)
        expect(req.request.method).toStrictEqual('DELETE')

        req.flush({}, {
          status: 204, statusText: "no content"
        })
      }));
    });
  });
});
