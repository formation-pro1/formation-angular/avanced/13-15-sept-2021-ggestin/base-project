import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Client} from "../../core/models/client";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private urlApi = environment.urlApi;

  constructor(
    private http: HttpClient
  ) { }

  list(): Observable<Client[]> {
    return this.http.get<Client[]>(`${this.urlApi}/clients`);
  }

  get(id: number): Observable<Client> {
    return this.http.get<Client>(`${this.urlApi}/clients/${id}`);
  }

  update(data: Client) {
    return this.http.put<Client>(`${this.urlApi}/clients/${data.id}`, data);
  }

  delete(data: Client): Observable<HttpResponse<void>> {
    return this.http.delete<void>(`${this.urlApi}/clients/${data.id}`, {
      observe: "response"
    });
  }

  create(data: Client): Observable<HttpResponse<Client>> {
    return this.http.post<Client>(`${this.urlApi}/clients`, data, {
      observe: "response"
    });
  }
}
