import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageAddClientComponent } from './pages/page-add-client/page-add-client.component';
import { PageEditClientComponent } from './pages/page-edit-client/page-edit-client.component';
import { PageListClientsComponent } from './pages/page-list-clients/page-list-clients.component';
import {ClientLayoutFullComponent} from "./layout/client-layout-full/client-layout-full.component";

const routes: Routes = [{
  path: '', component: ClientLayoutFullComponent, children: [
    {path: '', component: PageListClientsComponent, data: {pageTitle: 'LIST'}},
    {path: 'edit/:id', component: PageEditClientComponent, data: {pageTitle: 'EDIT'}},
    {path: 'add', component: PageAddClientComponent, data: {pageTitle: 'ADD'}},
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
