import { Pipe, PipeTransform } from '@angular/core';
import { Order } from 'src/app/core/models/order';

@Pipe({
  name: 'totalTva'
})
export class TotalPipe implements PipeTransform {

  transform(value: Order, tva?: boolean): number {
    if(tva) {
      return value.totalTTC()
    }
    return value.totalHT();
  }

}
