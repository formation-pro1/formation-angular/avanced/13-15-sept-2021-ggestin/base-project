import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import {BehaviorSubject, Observable} from "rxjs";
import {map} from "rxjs/operators";

interface User {
  login: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);

  get isLogged$(): Observable<boolean> {
    return this.user.pipe(map(user => user !== null));
  }

  get user$(): Observable<User | null> {
    return this.user.asObservable();
  }

  constructor(
    public router: Router,
  ) {
  }

  public login(login: string) {
    this.user.next({login});
    this.router.navigate(['/clients']);
  }

  logout() {
    this.user.next(null);
    this.router.navigate(['/sign-in']);
  }
}
