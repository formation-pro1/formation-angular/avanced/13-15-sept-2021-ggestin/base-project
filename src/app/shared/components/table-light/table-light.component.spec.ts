import {ComponentFixture, TestBed} from '@angular/core/testing';

import { TableLightComponent } from './table-light.component';
import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";

describe('TableLightComponent', () => {
  let component: TableLightComponent;
  let fixture: ComponentFixture<TableLightComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableLightComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableLightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  const getAllTr = (deComp: DebugElement): HTMLTableCellElement[] => {
    return deComp.queryAll(By.css('table thead tr th')).map(de => de.nativeElement);
  }

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
    test('should not display headers', () => {
      expect(getAllTr(fixture.debugElement).length).toEqual(0);
    });
  });
  describe('Template test', () => {
    test('should have a label', () => {
      component.headers = ['h1', 'h2'];
      fixture.detectChanges();
      const e: HTMLTableCellElement[] = getAllTr(fixture.debugElement)
      expect(e.length).toEqual(component.headers.length);
      e.forEach((element, index) =>
        //@ts-ignore
        expect(element.textContent.trim()).toStrictEqual(component.headers[index])
      )
    });
  });
});
