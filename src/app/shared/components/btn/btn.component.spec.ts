import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BtnComponent} from './btn.component';
import {RouterTestingModule} from "@angular/router/testing";
import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";

describe('BtnComponent', () => {
  let component: BtnComponent;
  let fixture: ComponentFixture<BtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BtnComponent],
      imports: [
        RouterTestingModule
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
    test('should not display button', () => {
      const deComp: DebugElement = fixture.debugElement;
      expect(deComp.query(By.css('button'))).toBeNull();
    });
  });
  describe('Template test', () => {
    test('should have a label', () => {
      component.label = 'toto';
      fixture.detectChanges();
      const deComp: DebugElement = fixture.debugElement;
      
      expect(deComp.query(By.css('button'))).not.toBeNull();
      const e: HTMLElement = deComp.query(By.css('button')).nativeElement;
      // @ts-ignore
      expect(e.textContent.trim()).toStrictEqual('toto');
    });
  });
});
