import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-btn',
  template: `<button *ngIf="label" [routerLink]="route">{{label}}</button>`
})
export class BtnComponent {

  @Input()
  public label!: string;

  @Input()
  public route!: string;
}
