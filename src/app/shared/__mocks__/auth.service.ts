import {BehaviorSubject} from "rxjs";

export const authServiceMock = {
  isLogged$: new BehaviorSubject(true),
  user$: new BehaviorSubject({login: 'loginTest'}),
  login: jest.fn((login: string) => {}),
  logout: jest.fn(() => {})
}
