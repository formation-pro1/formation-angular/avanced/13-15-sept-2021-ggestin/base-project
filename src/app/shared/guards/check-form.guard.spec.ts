import { TestBed } from '@angular/core/testing';

import { CheckFormGuard } from './check-form.guard';

describe('CheckFormGuard', () => {
  let guard: CheckFormGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CheckFormGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
