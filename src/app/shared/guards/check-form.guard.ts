import {Directive, Injectable, ViewChild} from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {PageEditOrderComponent} from "../../orders/pages/page-edit-order/page-edit-order.component";
import {PageAddOrderComponent} from "../../orders/pages/page-add-order/page-add-order.component";
import {FormGroup} from "@angular/forms";

export abstract class HasFormDirty {

  public form!: FormGroup;

  isFormDirty(): boolean {
    return this.form.dirty
  }
}

@Directive()
export abstract class CheckForm<T extends HasFormDirty> {

  @ViewChild('form')
  private formComponent!: T;

  public isFormDirty(): boolean {
    return this.formComponent.isFormDirty();
  }
}

@Injectable({
  providedIn: 'root'
})
export class CheckFormGuard implements CanDeactivate<CheckForm<any>> {
  canDeactivate(
    component: CheckForm<any>,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (component.isFormDirty()) {
      if (confirm("Voulez vraiment quitter la route comme Frodon ?")) {
        return confirm("Vraiment ? t'es sur ?")
      } else {
        return false;
      }
    }
    return true;
  }
}
